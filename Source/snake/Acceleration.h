// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Acceleration.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API AAcceleration : public AFood
{
	GENERATED_BODY()
	
public:
	AAcceleration();

	virtual void Interact(AActor* Interactor, const bool bIsHead = false) override;

private:
	UPROPERTY(EditDefaultsOnly)
	float AccelerationSpeed;
};
