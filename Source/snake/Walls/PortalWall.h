// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Wall.h"
#include "snake/SnakeBase.h"

#include "PortalWall.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API APortalWall : public AWall
{
	GENERATED_BODY()
	
public:
	APortalWall();


public:
	virtual void Interact(AActor* Interactor, const bool bIsHead = false) override;
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	APortalWall* ExitPortal;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EMovementDirection ExitDirection;
};
