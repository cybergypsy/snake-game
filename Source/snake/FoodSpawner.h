// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Acceleration.h"
#include "FoodSpawner.generated.h"

class AFood;

UCLASS()
class SNAKE_API AFoodSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodSpawner();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION()
	void SpawnFood();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<AFood>> SpawnableFood;

private:

	UPROPERTY(EditDefaultsOnly)
	FVector LeftUpPoint;

	UPROPERTY(EditDefaultsOnly)
	FVector RightDownPoint;

	TSubclassOf<AFood> BakeFood();

};
