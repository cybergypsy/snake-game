// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.generated.h"


class ASnakeElementBase;


UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	RIGHT,
	LEFT,
	NONE
};

UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	float MaxSpeed;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY(EditDefaultsOnly)
	float MinSpeed;

public:
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
	EMovementDirection NextMoveDirection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector ArenaSize;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	void GameOver();

	float GetMovementSpeed() const { return MovementSpeed; }
	void SetMovementSpeed(float NewSpeed);

	void TeleportSnake(const FVector TeleportLocation, const EMovementDirection NewMoveDirection);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
